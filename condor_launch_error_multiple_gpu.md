## Error while queuing the job on condor

I was trying to send a job to train to condor while requesting 4 GPUs. However, it did crash due to a PermissionError while trying to launch multiple jobs with torch. Starting a job with torch.distributed.launch usually works while starting from a interactive terminal. 

- content of submit file `nuclei_SN_4gpu`

```bash
# Import environment variables
getenv       = true

# Full path to the program, without any arguments
executable   = /home/max/anaconda3/envs/pt_mask_Rcnn_env/bin/python

# Arguments (see below for syntax)
arguments    = "-m torch.distributed.launch --nproc_per_node=4 \
/home/max/github/maskrcnn-benchmark/tools/train_net.py \
--config-file /home/max/github/maskrcnn-benchmark/configs/test_coco_4gpu.yaml"

# Log file
log          = /home/max/mrcnn_b_work/OUT/nuclei_test_4.log

# stdout
output       = /home/max/mrcnn_b_work/OUT/nuclei_test_4.out

# stderr
error        = /home/max/mrcnn_b_work/OUT/nuclei_test_4.error

# Resource request (see below)
request_gpus=4

# Queue the job
queue
```

Comparably, the submit file is different when requesting only one GPU, as the `arguments` are changed:

```bash
# Full path to the program, without any arguments
executable   = /home/max/anaconda3/envs/pt_mask_Rcnn_env/bin/python

# Arguments (see below for syntax)
arguments    = "/home/max/github/maskrcnn-benchmark/tools/train_net.py \
--config-file /home/max/github/maskrcnn-benchmark/configs/test_coco_1gpu.yaml"
```

Here are the outputs of the files .out, .error and .log from the run of 4 GPUs:

- The output file `nuclei_test_4.out` is empty

- Error message in error-file `nuclei_test_4.error`

```bash
(base) [max@monod OUT]$ cat nuclei_test_4.error 
Traceback (most recent call last):
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/runpy.py", line 193, in _run_module_as_main
    "__main__", mod_spec)
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/runpy.py", line 85, in _run_code
    exec(code, run_globals)
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/site-packages/torch/distributed/launch.py", line 219, in <module>
    main()
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/site-packages/torch/distributed/launch.py", line 211, in main
    process = subprocess.Popen(cmd, env=current_env)
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/subprocess.py", line 729, in __init__
    restore_signals, start_new_session)
  File "/home/max/anaconda3/envs/pt_mask_Rcnn_env/lib/python3.6/subprocess.py", line 1364, in _execute_child
    raise child_exception_type(errno_num, err_msg, err_filename)
PermissionError: [Errno 13] Permission denied: '
```

- content of `nuclei_test_4.log`

```bash
(base) [max@monod OUT]$ cat nuclei_test_4.log 
000 (2557.000.000) 02/16 14:33:45 Job submitted from host: <193.10.16.58:9618?addrs=193.10.16.58-9618+[--1]-9618&noUDP&sock=10390_4118_4>
...
001 (2557.000.000) 02/16 14:33:46 Job executing on host: <192.168.0.31:9618?addrs=192.168.0.31-9618+[--1]-9618&noUDP&sock=137354_ff73_3>
...
006 (2557.000.000) 02/16 14:33:54 Image size of job updated: 46996
	0  -  MemoryUsage of job (MB)
	0  -  ResidentSetSize of job (KB)
...
005 (2557.000.000) 02/16 14:33:54 Job terminated.
	(1) Normal termination (return value 1)
		Usr 0 00:00:01, Sys 0 00:00:01  -  Run Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Run Local Usage
		Usr 0 00:00:01, Sys 0 00:00:01  -  Total Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Total Local Usage
	0  -  Run Bytes Sent By Job
	0  -  Run Bytes Received By Job
	0  -  Total Bytes Sent By Job
	0  -  Total Bytes Received By Job
	Partitionable Resources :    Usage  Request Allocated
	   Cpus                 :                 1         1
	   Disk (KB)            :    12500    12500   1344786
	   Gpus                 :                 4         4
	   Memory (MB)          :        0       13        13
...
000 (2560.000.000) 02/16 14:38:34 Job submitted from host: <193.10.16.58:9618?addrs=193.10.16.58-9618+[--1]-9618&noUDP&sock=10390_4118_4>
...
001 (2560.000.000) 02/16 14:38:35 Job executing on host: <192.168.0.31:9618?addrs=192.168.0.31-9618+[--1]-9618&noUDP&sock=137354_ff73_3>
...
006 (2560.000.000) 02/16 14:38:38 Image size of job updated: 12500
	0  -  MemoryUsage of job (MB)
	0  -  ResidentSetSize of job (KB)
...
005 (2560.000.000) 02/16 14:38:38 Job terminated.
	(1) Normal termination (return value 1)
		Usr 0 00:00:00, Sys 0 00:00:00  -  Run Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Run Local Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Total Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Total Local Usage
	0  -  Run Bytes Sent By Job
	0  -  Run Bytes Received By Job
	0  -  Total Bytes Sent By Job
	0  -  Total Bytes Received By Job
	Partitionable Resources :    Usage  Request Allocated
	   Cpus                 :                 1         1
	   Disk (KB)            :    12500    12500   1344786
	   Gpus                 :                 4         4
	   Memory (MB)          :        0       13        13
...
000 (2970.000.000) 02/21 13:48:50 Job submitted from host: <193.10.16.58:9618?addrs=193.10.16.58-9618+[--1]-9618&noUDP&sock=10390_4118_4>
...
001 (2970.000.000) 02/21 13:48:53 Job executing on host: <192.168.0.32:9618?addrs=192.168.0.32-9618+[--1]-9618&noUDP&sock=146834_7d45_3>
...
006 (2970.000.000) 02/21 13:49:02 Image size of job updated: 92384
	39  -  MemoryUsage of job (MB)
	39608  -  ResidentSetSize of job (KB)
...
006 (2970.000.000) 02/21 13:49:03 Image size of job updated: 94492
	39  -  MemoryUsage of job (MB)
	39608  -  ResidentSetSize of job (KB)
...
005 (2970.000.000) 02/21 13:49:03 Job terminated.
	(1) Normal termination (return value 1)
		Usr 0 00:00:01, Sys 0 00:00:01  -  Run Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Run Local Usage
		Usr 0 00:00:01, Sys 0 00:00:01  -  Total Remote Usage
		Usr 0 00:00:00, Sys 0 00:00:00  -  Total Local Usage
	0  -  Run Bytes Sent By Job
	0  -  Run Bytes Received By Job
	0  -  Total Bytes Sent By Job
	0  -  Total Bytes Received By Job
	Partitionable Resources :    Usage  Request Allocated
	   Cpus                 :                 1         1
	   Disk (KB)            :    12500    12500   1344793
	   Gpus                 :                 4         4
	   Memory (MB)          :       39       13        13
...
```
